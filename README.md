![](elvire.jpeg)

# ELVIRE: Electric Vehicle Communication to Infrastructure, Road Services and Electricity Supply

## 1. Executive Summary 

The ELVIRE project was the first research project partially funded by the European Commission which took a close look at the expected new market of e-Mobility and its needed technical improvements. Since Electrical Vehicles (EVs) are known for their limited driving range the adoption of these new cars was doubtful, although the media enthusiastically reported on this new possibility. The expectation was set, that EVs would soon replace the conventional cars.

However the limited driving range of EVs of about 100 to 150 kilometres needed to be overcome or the users of such cars would experience a phenomenon known as Range Anxiety, which is the fear to break down with an empty battery before the destination has been reached. Therefore Range Anxiety was seen as a market limiting factor of EVs.

The 39 months long project looked at solutions to overcome these problems, battery technology and wireless recharging while driving. The technologies investigated and the solutions developed came solely of the area of Information and Communication Technology (ICT).

First of all it was clear from the start, that EVs require new kinds of information and energy supply services so a Service Provider had to be established, a role which was taken by Better Place in our project. The service provider had to develop a scalable back-end system which would generate services and deal with EV user requests. Also there was the need of charge infrastructure and ideally this had to be full coverage. Better Place also filled this role of an infrastructure operator. The required connection to a utility grid was filled by ENDESA and CEA helped with the development of an energy demand prediction system. SAP looked at the possibilities of roaming and developed a clearing house so that vehicles could charge at foreign infrastructure without the need to sign an additional contract.

Vehicle Equipment was needed to provide a dashboard integrated communication and service terminal to the EV driver. Since the equipment was meant to be installed in vehicles the car manufacturers Renault and Volkswagen as well as automotive supplier Continental took this work package, accompanied by ATB from Bremen. The equipment was developed and cars were equipped so that open road tests could be conducted. Lindholmen Science Park verified the system successfully and the final demonstration presented it to the Community of Interest. 

## 2. Summary Description of the Project Context and Objectives

In the centre of this project stands the development of those Information and Communication Technologies (ICT) and Services that are needed to neutralize the electric vehicle (EV) driver's "Range Anxiety": the fear to break down due to the vehicle's driving range limitation and at the same time to cope with the sparse distribution of electrical supply points.

Therefore the objective of ELVIRE was to develop an on-board electric energy communication & service platform for realistic use-cases including the relevant external communication and services. for this purpose the following actions were taken:

+ selection representative use-cases according to realistic scenarios and business models

+ identification & development of ICT & services needed to comply with the use-cases

+ development of "prototypes" for on-board Communication and E-energy service unit

+ Verification of all integrated sub-systems on prototype level and demonstration of the proof of concept.

Based on a typical mission of an EV as use case, the project’s purpose was to develop a customer oriented, open service platform required for the optimum interaction between the user in his vehicle, the service provision layer and an intelligent electricity infrastructure. 

ELVIRE was structured into five work packages, with:

+ WP 1000 covering the project administration, legal aspects and dissemination and the inclusion of compementary RTD

+ WP 2000 defined relevant mission data for the use-cases in consideration of technical an commercial aspects

+ WP 3000 addressed the external ICT for EV service provision, charge infrastructure management and balancing e+ nergy demand and supply for the users

+ WP 4000 developed the EV Communication Device and the OEM-neural universal on-board communication and service platform

+ WP 5000 conducted usability tests and the everall end-to-end system validation     






